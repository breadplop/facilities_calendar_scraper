
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from datetime import datetime
from webdriver_manager.chrome import ChromeDriverManager

import pymongo
import pandas as pd

nusync_cal_frame = "https://calendar.google.com/calendar/embed?showPrint=0&shbs=0&showTz=0&height=600&wkst=1&bgcolor=%23FFFFFF&src=eusoffcalendar%40gmail.com&co%231B887A&ctz=Asia%2FSingapore"
options = webdriver.ChromeOptions()
options.add_argument('headless')
# driver = webdriver.Chrome("/Users/brenda/Desktop/Kode/bt3103/SynchroNUS/node_modules/chromedriver/lib/chromedriver/chromedriver", options=options)
driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)

def getSoup(url, driver, delay=3):
    #Go to Calendar
    driver.get(url)
    driver.find_element_by_id("tab-controller-container-agenda").click() #Agenda view for easier scraping

    check = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.CLASS_NAME, 'day'))) #webdriver will wait for a page to load by default but not for loading inside frames or for ajax requests, hence waiting is used explicitly using expected_conditions for the page to load
    data = driver.find_element_by_class_name("calendar-container").get_attribute('innerHTML')
    # event_dates = driver.find_element_by_class_name("date-label").get_attribute('innerHTML')
    soup = BeautifulSoup(data,'lxml')
    return soup
    
def getData(_soup):
    col_list = ['Date', 'Time', 'Event']
    df = pd.DataFrame(columns=col_list)
    
    event_days = _soup.findAll("div", {"class": "day"})
    for _day in event_days:
        tmp_event_list = _day.findAll('div', {'class': 'event-summary'})
        df_col1, df_col2, df_col3 = [], [], []
        
        for _event in tmp_event_list:
            _date = _event['id'][-8:]
            _date = datetime.strptime(_date, '%Y%m%d')
            _date = _date.strftime('%Y-%m-%d')
            _time = _event.span['title']
            event_name = _event.div.text
            df_col1.append(_date)
            df_col2.append(_time)
            df_col3.append(str(event_name.encode('ascii','ignore').decode('UTF-8')))
        
        tmp_df = pd.DataFrame(columns=col_list)
        tmp_df['Date'] = df_col1
        tmp_df['Time'] = df_col2
        tmp_df['Event'] = df_col3
        df = df.append(tmp_df)
    return df

def getDbClient():
    srv_address = 'mongodb://brendathng:eusoffhall@cluster0-shard-00-00-dbct0.mongodb.net:27017,cluster0-shard-00-01-dbct0.mongodb.net:27017,cluster0-shard-00-02-dbct0.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true'
    client = pymongo.MongoClient(srv_address)
    return client

def insertToDB(df, client, db_name='facilitiescalendar', collection_name = 'collection1'):
    srv_address = 'mongodb://brendathng:eusoffhall@cluster0-shard-00-00-dbct0.mongodb.net:27017,cluster0-shard-00-01-dbct0.mongodb.net:27017,cluster0-shard-00-02-dbct0.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true'
    client = pymongo.MongoClient(srv_address)
    db = client[db_name]
    collection = db[collection_name]
    # check if already inserted
    today = datetime.today().strftime('%Y-%m-%d')
    find = collection.find_one({'Date': today})
    if find != None:
        print('already inserted')
    else:
        collection.insert_many(df.to_dict('records'))
        print('inserted!')
    


if __name__ == "__main__":
    #script to pull and add data to database. to run a cronjob weekly.
    soup2 = getSoup(nusync_cal_frame, driver, delay=3)
    df = getData(soup2)
    print(df.head())
    insertToDB(df, getDbClient())
    
    
